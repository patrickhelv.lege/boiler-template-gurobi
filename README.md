# Boiler-Template-Gurobi



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Requirements 

pip install virtualenv

# Install a venv
- python -m venv env
- python3 -m venv env

cd .env\Scripts\
./activate.bat
./Activate.ps1

![bilde.png](./bilde.png)

# When it is green run
- pip install -r requirements.txt

## If you want to install gurobi with pip

https://support.gurobi.com/hc/en-us/articles/360059842732-How-do-I-set-up-a-license-without-installing-the-full-Gurobi-package-

## Retrieving the licence key to use for PIP gurobi

https://www.gurobi.com/documentation/9.5/quickstart_linux/retrieving_and_setting_up_.html#section:RetrieveLicense


## For each time you have to make sure that you are in the virtual env

cd .env\Scripts\
./activate.bat
./Activate.ps1

Can check want env you are running in vscode
![bilde-1.png](./bilde-1.png)

